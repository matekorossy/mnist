import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

(train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.fashion_mnist.load_data()

print("Fashion MNIST Project with tensorflow {}".format(tf.__version__))

# scale values (0 to 1)
train_images = train_images / 255.0
test_images = test_images / 255.0

class_names = ["T-Shirt", "Trouser", "Pullover", "Dress", "Coat", "Sandal", "Shirt", "Sneaker", "Bag", "Ankle Boot"]

# training data sample
plt.figure(0).canvas.set_window_title("Training data sample")
for i in range(9):
    plt.subplot(3, 3, i+1)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(train_images[i], cmap="gist_yarg")
    plt.colorbar()
    plt.xlabel(class_names[train_labels[i]])
plt.show()

# setup the model
mymodel = tf.keras.Sequential()

mymodel.add(tf.keras.layers.Flatten(input_shape=(28,28)))
mymodel.add(tf.keras.layers.Dense(units=128, activation="relu"))
mymodel.add(tf.keras.layers.Dense(units=10, activation="softmax"))

mymodel.compile(optimizer="adam", loss="sparse_categorical_crossentropy", metrics=["accuracy"])

# train the model
mymodel.fit(train_images, train_labels, epochs=15)

# evaluate the model
loss, accuracy = mymodel.evaluate(test_images, test_labels, verbose=2)
print("\nTest loss: {}".format(loss))
print("Test accuracy {}".format(accuracy))


print("\nSummary of my model:")
print(mymodel.summary())

# saving model
mymodel.save("fashion_mnist_model.h5")
